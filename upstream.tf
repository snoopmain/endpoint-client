data "terraform_remote_state" "vpc" {
  backend = "remote"
  config = {
    organization = "mark-snoop"
    workspaces = {
      name = "prod-endpoint-service"
    }
  }
}

locals {
    endpoint_service = data.terraform_remote_state.vpc.outputs.endpoint_service
}