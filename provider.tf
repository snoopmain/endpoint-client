provider "aws" {
  region = "ap-southeast-2"
}

terraform {
   cloud {
    organization = "mark-snoop"
    workspaces {
    name = "prod-endpoint-client"
    }
   }
 }