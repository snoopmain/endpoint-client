resource "aws_vpc_endpoint" "poc" {
  vpc_id             = var.vpc
  service_name       = local.endpoint_service
  vpc_endpoint_type  = "Interface"
  security_group_ids = [aws_security_group.endpoint.id]
  subnet_ids         = [
    var.subnet1,
    var.subnet2]
}