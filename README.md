This file deploys an endpoint that connects to the endpoint service specified in upstream.tf.
A prerequisite to this successfully deploying is the account id must be added to the endpoint service as an allowed principal. 
Required input variables for this deployment are:
vpc: the vpc the endpoint will be deployed into
subnet1 & subnet2: these subnets MUST be in azse2-az1 and azse2-az3. This is not optional as the Privatelink protocol cannot handle a difference in subnets at either end of the tunnel.

This deployment produces one output and that is the collection of DNS names of the endpoints. These can be used from any availability zone.