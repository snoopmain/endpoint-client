Refactor endpoint service to be an input variable.

Refactor outputs to give friendly DNS names.

VPC and Subnet lookup by tags. Elegant solution is for this repo to query a given account for "x_endpoint_service_here" tag. Account owner only needs to tag destination subnets with this and the deployment will require no input variables. 

Try to make documentation a little lighter on the reader. Perhaps can remove vars.auto.tfvars.example.