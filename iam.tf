data "aws_iam_policy_document" "ssm" {
  statement {
    actions = [
      "ssm:UpdateInstanceInformation",
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel",
      "s3:GetEncryptionConfiguration"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "ssm" {
  name   = "SSM"
  policy = data.aws_iam_policy_document.ssm.json
}

resource "aws_iam_role" "ssm" {
  assume_role_policy = data.aws_iam_policy_document.ssm-instance-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "ssm" {
  role       = aws_iam_role.ssm.name
  policy_arn = aws_iam_policy.ssm.arn
}

data "aws_iam_policy_document" "ssm-instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}